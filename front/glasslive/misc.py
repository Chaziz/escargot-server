from typing import Optional, Tuple, Any, Iterable
from hashlib import md5, sha1
import hmac
from pytz import timezone
from datetime import datetime
import base64
import binascii
from quopri import encodestring as quopri_encode
import struct
from enum import Enum

from util.misc import first_in_iterable, date_format, DefaultDict
from typing import Optional

from core import error
from core.backend import Backend, BackendSession
from core.models import User, OIM, Substatus, NetworkID, GroupChatState, GroupChat

class GlassStatus(Enum):
    offline = object()
    online = object()
    away = object()
    busy = object()

    @classmethod
    def ToSubstatus(cls, glass_status: 'GlassStatus') -> Substatus:
        return _ToSubstatus[glass_status]
    
    @classmethod
    def FromSubstatus(cls, substatus: 'Substatus') -> 'GlassStatus':
        return _FromSubstatus[substatus]

_ToSubstatus = DefaultDict(Substatus.Busy, {
    GlassStatus.offline: Substatus.Offline,
    GlassStatus.online: Substatus.Online,
    GlassStatus.away: Substatus.Away,
    GlassStatus.busy: Substatus.Busy,
})

_FromSubstatus = DefaultDict(GlassStatus.busy, {
    Substatus.Offline: GlassStatus.offline,
    Substatus.Online: GlassStatus.online,
    Substatus.Busy: GlassStatus.busy,
    Substatus.Idle: GlassStatus.away,
    Substatus.BRB: GlassStatus.away,
    Substatus.Away: GlassStatus.away,
    Substatus.OnPhone: GlassStatus.busy,
    Substatus.OutToLunch: GlassStatus.away,
    Substatus.Invisible: GlassStatus.offline,
    Substatus.NotAtHome: GlassStatus.away,
    Substatus.NotAtDesk: GlassStatus.away,
    Substatus.NotInOffice: GlassStatus.away,
    Substatus.OnVacation: GlassStatus.away,
    Substatus.SteppedOut: GlassStatus.away,
})

def encode_email_pop(email: str, pop_id: Optional[str]) -> str:
	result = email
	if pop_id:
		result = '{};{}'.format(result, '{' + pop_id + '}')
	return result

def decode_email_pop(s: str) -> Tuple[str, Optional[str]]:
	# Split `foo@email.com;{uuid}` into (email, pop_id)
	parts = s.split(';', 1)
	if len(parts) < 2:
		pop_id = None
	else:
		pop_id = parts[1]
	return (parts[0], pop_id)

def normalize_pop_id(pop_id: str) -> str:
	return ''.join(pop_id.replace('{', '', 1).rsplit('}', 1))