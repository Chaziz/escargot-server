from contextlib import nullcontext
from telnetlib import theNULL
from typing import Tuple, Dict, Any, Optional, List
from datetime import datetime
import base64
import secrets
import hmac
import re
import binascii
import struct
import time
import requests

import asyncio

from util.misc import Logger, date_format, MultiDict
import settings

from core import event, error
from core.backend import Backend, BackendSession, Chat, ChatSession
from core.models import (
    Substatus, Lst, NetworkID, User, Group, GroupChat, GroupChatRole, GroupChatState,
    Contact, TextWithData, MessageData, MessageType, LoginOption, OIM,
)
from core.client import Client

from .glass import GlassCtrl

from .misc import (
    GlassStatus, encode_email_pop, decode_email_pop, normalize_pop_id
)

class GlassCtrlLive(GlassCtrl):
    __slots__ = (
        'backend', 'dialect', 'client', 'usr_blid', 'usr_name', 'bs', 'cs'
    )

    backend: Backend
    dialect: int
    client: Client
    usr_blid: Optional[int]
    usr_name: Optional[str]
    bs: Optional[BackendSession]
    cs: Optional[ChatSession]

    def __init__(self, logger: Logger, via: str, backend: Backend) -> None:
        super().__init__(logger)
        self.backend = backend
        self.dialect = 0
        self.client = Client('blg', '?', via)
        self.usr_blid = 0
        self.usr_name = "BlockheadDummy"
        self.bs = None
        self.cs = None
        
    def _friendsList(self) -> None:
        bs = self.bs
        assert bs is not None
        user = bs.user
        detail = user.detail
        assert detail is not None
        contacts = detail.contacts

        list = []

        for lst in (Lst.FL, Lst.AL, Lst.RL):
            contactlist = [c for c in contacts.values() if c.lists & lst]
            if contactlist:
                for i, c in enumerate(contactlist):
                    friend_name = c.status.name
                    friend_blid = c.head.blockland_id
                    friend_status = GlassStatus.FromSubstatus(c.head.status.substatus).name
                    friend = dict(username=friend_name, blid=friend_blid, status=friend_status, icon="user")
                    list.append(friend)
        self.send_reply(dict(type="friendsList", friends=list))

    def _roomsList(self) -> None:
        backend = self.backend
        assert backend is not None

        list = []

        for groupchats in backend.user_service.get_groupchat_batch(self.bs.user):
            group_id = groupchats.int_id
            group_title = groupchats.name
            group_users = len(groupchats.memberships)
            room = dict(id=group_id, title=group_title, users=group_users, image="user")
            list.append(room)
        self.send_reply(dict(type="roomList", rooms=list))

    def _m_auth(self, *args: str) -> None:
        for key, value in args: # g3 and g4 move the json stuff quite a bit. so we need to do this.
            if key == "blid":
                self.usr_blid = int(value)
                self.usr_name = 'Blockhead' + value # placeholder username
            if key == "version":
                version = value
                self.client = Client('blg', value, self.client.via)
            if key == "ident":
                ident = value
        ip = requests.get('http://ipinfo.io/json').json()['ip']
        response = requests.get("http://api.blocklandglass.com/api/3/authCheck.php?ident=" + ident + "&ip=" + ip)
        print(response.json())
        self.usr_name = response.json()['username']  # actual blockland username
        self.dialect = int(version.split("-", 1)[0].replace('.', ''))
        uuid = self.backend.user_service.get_uuid_blockland_id(self.usr_blid) # blg does not use passwords
        if self.dialect < 400 or self.dialect == 433: # glass 3 and tempered glass
            if uuid is not None:
                self.bs = self.backend.login(uuid, self.client, BackendEventHandler(self.backend.loop, self), option = LoginOption.BootOthers)
                self.send_reply(dict(type="auth", status="success"))
                self._friendsList() # get friends list, moved to make code more easier to read.

                self.bs.me_update({ 'substatus': Substatus.Online })
                self.bs.me_update({ 'name': self.usr_name })
            else:
                self.send_reply(dict(type="auth", status="failed"))
                self.send_reply(dict(type="error", message="Escargot GRKB Server - UUID not found, no account linked to BL_ID?", showDialog=True))
        else:
            self.send_reply(dict(type="auth", status="failed"))
            self.send_reply(dict(type="error", message="Escargot GRKB Server - This version of Blockland Glass is not compatible.", showDialog=True))

    def _m_setStatus(self, *args: str) -> None:
        status = args[0][1]
        substatus = GlassStatus.ToSubstatus(getattr(GlassStatus, status))
        self.bs.me_update({ 'substatus': substatus })

    def _m_avatar(self, *args: str) -> None:
        pass

    def _m_updateLocation(self, *args: str) -> None:
        pass

    def _m_friendRequest(self, *args: str) -> None:
        target = args[0][1]
        contact_uuid = self.backend.user_service.get_uuid_blockland_id(target) # is user_service shit related to auth?
        try:
            self.bs.me_contact_add(contact_uuid, 0x02, name = "BLG") # todo: confirm if this works
        except Exception as ex:
            print(ex)
        return

    def _m_ping(self, *args: str) -> None:
        self.send_reply(dict(type="pong", key=args[0][1]))

    # FIXME:  this is a bit dumb
    def _m_messageTyping(self, *args: str) -> None:
        target_blockland_id = int(args[0][1])
        uuid = self.backend.user_service.get_uuid_blockland_id(target_blockland_id)
        invitee = self.backend._load_user_record(uuid)
        chat = self.backend.chat_create()
        bs = self.bs
        cs = chat.join('blg', bs, ChatEventHandler(self.backend.loop, self, bs))
        self.cs = cs
        cs.invite(invitee)

    def _m_message(self, *args: str) -> None:
        message = args[0][1]
        target_blockland_id = int(args[1][1])
        cs = self.cs
        cs.send_message_to_everyone(MessageData(
            sender = self.cs.user,
            type = MessageType.Chat,
            text = message,
        ))

    def _m_roomJoin(self, *args: str) -> None:
        bs = self.bs
        roomid = args[0][1]
        roominfo = self.backend.user_service.get_groupchat_int(roomid)
        cs = self.backend.join_groupchat(roominfo.chat_id, 'blg', self.bs, GroupChatEventHandler(self), pop_id = self.bs.front_data.get('msn_pop_id'))
        self.send_reply(dict(type="roomJoin", title = roominfo.name, id = roomid, icon = "user", motd = roominfo.name))
        chat = cs.chat
        chat.send_participant_joined(cs, initial_join = True)
        
    def _m_roomChat(self, *args: str) -> None:
        backend = self.backend
        bs = self.bs
        messageText = args[0][1]
        roomid = args[1][1]
        room = self.backend.user_service.get_groupchat_int(roomid)
        cs = backend.get_groupchat_cs(room.chat_id, bs)
        message = MessageData(sender = bs.user, sender_pop_id = bs.front_data.get('msn_pop_id'), type = MessageType.Chat, text = messageText)
        cs.send_message_to_everyone(message)
        self.send_reply(dict(type="roomMessage", room=roomid, sender=self.usr_name, sender_id=self.usr_blid, msg=messageText))

    def _m_getRoomList(self, *args: str) -> None:
        self._roomsList()

    def _on_close(self) -> None:
        if self.bs:
            self.bs.close()
        if self.cs:
            self.cs.close()

    def on_connect(self) -> None:
        pass

class BackendEventHandler(event.BackendEventHandler):
    __slots__ = ('loop', 'ctrl', 'bs')

    loop: asyncio.AbstractEventLoop
    ctrl: GlassCtrl
    bs: BackendSession

    def __init__(self, loop: asyncio.AbstractEventLoop, ctrl: GlassCtrl) -> None:
        self.loop = loop
        self.ctrl = ctrl

    def on_open(self) -> None:
        pass
    
    def on_presence_notification(
        self, ctc: Contact, on_contact_add: bool, old_substatus: Substatus, *,
        trid: Optional[str] = None, update_status: bool = True, update_info_other: bool = True,
        send_status_on_bl: bool = False, sess_id: Optional[int] = None, updated_phone_info: Optional[Dict[str, Any]] = None,
    ) -> None:
        bs = self.ctrl.bs
        assert bs is not None
        user = bs.user

        if update_status:
            status = GlassStatus.FromSubstatus(ctc.status.substatus)
            self.ctrl.send_reply(dict(type="friendStatus", username=ctc.status.name, blid=ctc.head.blockland_id, status=status.name))
            ##self.ctrl.send_reply('NOTICE', ":{} is now {}".format(ctc.head.email, ctc.status.substatus))
            return
        
    
    def on_presence_self_notification(
        self, old_substatus: Substatus, *, update_status: bool = True, update_info: bool = True,
    ) -> None:
        print("ass")
    
    def on_groupchat_created(self, groupchat: GroupChat) -> None:
        pass
    
    def on_groupchat_updated(self, groupchat: GroupChat) -> None:
        pass
    
    def on_left_groupchat(self, groupchat: GroupChat) -> None:
        pass
    
    def on_accepted_groupchat_invite(self, groupchat: GroupChat) -> None:
        pass
    
    def on_groupchat_invite_revoked(self, chat_id: str) -> None:
        pass
    
    def on_groupchat_role_updated(self, chat_id: str, role: GroupChatRole) -> None:
        pass
    
    def on_chat_invite(
        self, chat: Chat, inviter: User, *, group_chat: bool = False, inviter_id: Optional[str] = None, invite_msg: Optional[str] = None,
    ) -> None:
        bs = self.bs
        assert bs is not None
        evt = ChatEventHandler(self.loop, self.ctrl, self.bs)
        cs = chat.join('BLG', bs, evt)
        chat.send_participant_joined(cs)

    def on_declined_chat_invite(self, chat: Chat, group_chat: bool = False) -> None:
        pass
    
    def on_added_me(self, user: User, *, adder_id: Optional[str] = None, message: Optional[TextWithData] = None) -> None:
        pass
    
    def on_removed_me(self, user: User) -> None:
        pass
    
    def on_contact_request_denied(self, user_added: User, message: Optional[str], *, contact_id: Optional[str] = None) -> None:
        pass
    
    def on_oim_sent(self, oim: 'OIM') -> None:
        pass
    
    def on_login_elsewhere(self, option: LoginOption) -> None:
        pass
    
    def ymsg_on_p2p_msg_request(self, sess_id: int, yahoo_data: MultiDict[bytes, bytes]) -> None:
        pass
    
    def ymsg_on_xfer_init(self, sess_id: int, yahoo_data: MultiDict[bytes, bytes]) -> None:
        pass
    
    def ymsg_on_sent_ft_http(self, yahoo_id_sender: str, url_path: str, upload_time: float, message: str) -> None:
        pass

class ChatEventHandler(event.ChatEventHandler):
    __slots__ = ('loop', 'ctrl', 'bs', 'cs')
    
    loop: asyncio.AbstractEventLoop
    ctrl: GlassCtrl
    bs: BackendSession
    cs: ChatSession
    
    def __init__(self, loop: asyncio.AbstractEventLoop, ctrl: GlassCtrl, bs: BackendSession) -> None:
        self.loop = loop
        self.ctrl = ctrl
        self.bs = bs

    def on_participant_joined(self, cs_other: ChatSession, first_pop: bool, initial_join: bool) -> None:
        pass

    def on_participant_left(self, cs_other: ChatSession, last_pop: bool) -> None:
        pass

    def on_chat_invite_declined(
        self, chat: Chat, invitee: User, *, invitee_id: Optional[str] = None, message: Optional[str] = None, group_chat: bool = False,
    ) -> None:
        pass
    
    def on_chat_updated(self) -> None:
        pass
    
    def on_chat_roster_updated(self) -> None:
        pass
    
    def on_participant_status_updated(self, cs_other: ChatSession, first_pop: bool, initial: bool, old_substatus: Substatus) -> None:
        pass
    
    def on_message(self, data: MessageData) -> None:
        if data.text is None:
            return
        if data.type is MessageType.Chat:
            if data.text is None:
                return
            self.ctrl.send_reply(dict(type="message", message=data.text, sender=data.sender.status.name, sender_id=data.sender.blockland_id))
        if data.type is MessageType.Nudge:
            if data.text is None:
                return
            self.ctrl.send_reply(dict(type="message", message="Nudge!", sender=data.sender.status.name, sender_id=data.sender.blockland_id))
    
    def on_close(self) -> None:
        self.ctrl.close()

class GroupChatEventHandler(event.ChatEventHandler):
    __slots__ = ('ctrl', 'cs')
    
    ctrl: GlassCtrlLive
    cs: ChatSession

    def __init__(self, ctrl: GlassCtrlLive) -> None:
        self.ctrl = ctrl

    def on_close(self) -> None:
        print("group chat closed? [BLG]")

    def on_participant_joined(self, cs_other: ChatSession, first_pop: bool, initial_join: bool) -> None: pass
    def on_participant_left(self, cs_other: ChatSession, last_pop: bool) -> None: pass
    def on_chat_invite_declined(
        self, chat: Chat, invitee: User, *, invitee_id: Optional[str] = None,
        message: Optional[str] = None, group_chat: bool = False,
    ) -> None: pass
    def on_chat_updated(self) -> None: pass
    def on_chat_roster_updated(self) -> None: pass
    def on_participant_status_updated(self, cs_other: ChatSession, first_pop: bool, initial: bool, old_substatus: Substatus) -> None: pass
    def on_invite_declined(self, invited_user: User, *, invited_id: Optional[str] = None, message: str = '') -> None: pass
    def on_message(self, data: MessageData) -> None:
        if data.type is MessageType.Chat:
            self.ctrl.send_reply(dict(type="roomMessage", room=2, sender=data.sender.status.name, sender_id=data.sender.blockland_id, msg=str(data.text or '')))